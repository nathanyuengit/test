<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// User api route

Route::get('/v1/user/maxid', 'UserController@maxid')->name('user.maxid');
	
Route::post('/v1/login', 'UserController@login')->name('user.login');

Route::middleware('auth:api')->post('/v1/logout', 'UserController@logout')->name('user.logout');

Route::middleware('auth:api')->get('/v1/user/all', 'UserController@all')->name('user.all');

Route::middleware('auth:api')->get('/v1/user/{user}', 'UserController@show')->name('user.show');

Route::middleware('auth:api')->delete('/v1/user/{user}', 'UserController@destroy')->name('user.destroy');

Route::middleware('auth:api')->post('/v1/user', 'UserController@store')->name('user.store');

Route::middleware('auth:api')->put('/v1/user/{user}', 'UserController@update')->name('user.update');

// Task api route

Route::get('/v1/task/maxid', 'TaskController@maxid')->name('tasks.maxid');

Route::middleware('auth:api')->get('/v1/task/all', 'TaskController@all')->name('tasks.all');

Route::middleware('auth:api')->get('/v1/task/{task}', 'TaskController@show')->name('tasks.show');

Route::middleware('auth:api')->delete('/v1/task/{task}', 'TaskController@destroy')->name('tasks.destroy');

Route::middleware('auth:api')->post('/v1/task', 'TaskController@store')->name('tasks.store');

Route::middleware('auth:api')->put('/v1/task/{task}', 'TaskController@update')->name('tasks.update');

