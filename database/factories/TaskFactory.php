<?php

use App\Task;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

$factory->define(Task::class, function (Faker $faker) {
    return [
        //
    	'user_id' => App\User::inRandomOrder()->first()->id,
    	'description' => Str::random(20),
    ];
});
