<?php

namespace App\Http\Controllers;

use App\Task;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    // show all tasks 
	public function all()
	{
		$tasks = Task::orderBy('id','asc')->get();
		return response()->json($tasks);
	}
	
	// create task
	public function store(Request $request)
	{
		$request->validate([
			'user_id' => 'required',
			'description' => 'required'
		]);
		
		$task = Task::create($request->all());
		
		return response()->json([
			'msg' => 'Successfully created new task '.$task->id.'!',
			'task' => $task
		]);
	}
	
	// show specific task
	public function show(Task $task)
	{
		return $task;
	}
	
	// update task 
	public function update(Request $request, Task $task)
	{
		$request->validate([
			'title'       => 'nullable',
			'description' => 'nullable'
		]);
		
		$task->update($request->all());
		
		return response()->json([
			'msg' => 'Successfully updated task '.$task->id.'!',
			'task' => $task
		]);
	}
	
	// delete task
	public function destroy(Task $task)
	{
		$task->delete();
		
		return response()->json([
			'msg' => 'Successfully deleted task '.$task->id.'!',
		]);
	}
	
	// get max id of task
	public function maxid()
	{
		return response()->json([
			'id' => Task::max('id'),
		]);
	}
}
