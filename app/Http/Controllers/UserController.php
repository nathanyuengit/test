<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    //
	public function login(Request $request)
    {
    	if (auth()->attempt(['email' => $request->input('email'), 'password' => $request->input('password')])) {
    		// Authentication passed...
    		$user = auth()->user();
    		$user->api_token = str_random(60);
    		$user->save();
    		return $user;
    	}
    	
    	return response()->json([
    		'error' => 'Unauthenticated user',
    		'code' => 401,
    	], 401);
    	
    }
    
    public function logout(Request $request) {
    	
    	if (auth()->user()) {
    		$user = auth()->user();
    		$user->api_token = null; // clear api token
    		$user->save();
    		
    		return response()->json([
    			'msg' => 'Successfully logouted with user '.$user->id.'!',
    		]);
    	}
    	
    	return response()->json([
    		'error' => 'Logout user failed!',
    		'code' => 401,
    	], 401);
    	
    }
    
	public function all()
	{
		$users = User::orderBy('id','asc')->get();
		return response()->json($users);
	}
	
	public function store(Request $request)
	{
		$request->validate([
			'email'      => 'required',
			'name'       => 'required',
			'password'   => 'required',
			'first_name' => 'required',
			'last_name'  => 'required',
		]);
		
		$user = User::create(array_filter([
			'name'       => $request->input('name'),
			'email'      => $request->input('email'),
			'first_name' => $request->input('first_name'),
			'last_name'  => $request->input('last_name'),
			'password'   => Hash::make($request->input('password')),
		]));
		
		return response()->json([
			'msg' => 'Successfully created new user '.$user->id.'!',
			'user'    => $user
		]);
	}
	
	public function show(User $user)
	{
		return $user;
	}
	
	public function update(Request $request, User $user)
	{
		$request->validate([
			'email'      => 'nullable',
			'name'       => 'nullable',
			'first_name' => 'nullable',
			'last_name'  => 'nullable',
			'password'   => 'nullable',
		]);
		
		$user->update(array_filter([
			'name'       => $request->input('name'),
			'email'      => $request->input('email'),
			'first_name' => $request->input('first_name'),
			'last_name'  => $request->input('last_name'),
			'password'   => Hash::make($request->input('password')),
		]));
		
		return response()->json([
			'msg' => 'Successfully updated user '.$user->id.'!',
			'user' => $user
		]);
	}
	
	public function destroy(User $user)
	{
		$user->delete();
		
		return response()->json([
			'msg' => 'Successfully deleted user '.$user->id.'!',
		]);
	}
	
	public function maxid()
	{
		return response()->json([
			'id' => User::max('id'),
		]);
	}
}
